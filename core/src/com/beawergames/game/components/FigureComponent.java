package com.beawergames.game.components;

import com.badlogic.ashley.core.Component;
import com.beawergames.game.util.Figure;

public class FigureComponent implements Component {
    public Figure figure;

    public FigureComponent() {
        figure = new Figure();
    }

}
