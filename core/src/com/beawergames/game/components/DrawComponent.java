package com.beawergames.game.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class DrawComponent implements Component {
    public TextureRegion textureRegion;
    public PolygonSprite polygonSprite;

    public DrawComponent(Texture texture) {
        textureRegion = new TextureRegion(texture);
    }

    public DrawComponent(TextureRegion textureRegion) {
        this.textureRegion = textureRegion;
    }

    public DrawComponent(PolygonSprite polygonSprite) {
        this.polygonSprite = polygonSprite;
    }


}
