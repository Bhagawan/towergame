package com.beawergames.game.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector3;

public class TransformComponent implements Component {
    public Vector3 position;
    public float angle = 0.0f;
    public float width = 1.0f;
    public float height = 1.0f;
    public float scaleX = 1.0f;
    public float scaleY = 1.0f;

    public TransformComponent(float x, float y) {
        position = new Vector3(x, y, 0.0f);
    }

    public void setSize(float width, float height) {
        this.width = width;
        this.height = height;
    }

    public void setPosition(float x, float y) {
        position.x = x;
        position.y = y;
    }
}
