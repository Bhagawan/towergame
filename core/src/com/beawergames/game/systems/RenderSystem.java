package com.beawergames.game.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.beawergames.game.components.DrawComponent;
import com.beawergames.game.components.TransformComponent;

import java.util.Comparator;

public class RenderSystem extends IteratingSystem {
    private SpriteBatch batch;
    private PolygonSpriteBatch polygonSpriteBatch = new PolygonSpriteBatch();
    private ComponentMapper<DrawComponent> dM;
    private ComponentMapper<TransformComponent> tM;

    private Array<Entity> textureQueue;
    private Array<Entity> spriteQueue;
    private Comparator<Entity> comparator;

    public RenderSystem(SpriteBatch spriteBatch, int priority) {
        super(Family.all(DrawComponent.class, TransformComponent.class).get(), priority);
        batch = spriteBatch;

        dM = ComponentMapper.getFor(DrawComponent.class);
        tM = ComponentMapper.getFor(TransformComponent.class);

        textureQueue = new Array<>();
        spriteQueue = new Array<>();
        comparator = new Comparator<Entity>() {
            @Override
            public int compare(Entity entityA, Entity entityB) {
                return (int)Math.signum(tM.get(entityA).position.z - tM.get(entityB).position.z);
            }
        };

    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);

        textureQueue.sort(comparator);
        spriteQueue.sort(comparator);

        TransformComponent tC;
        DrawComponent dC;

        polygonSpriteBatch.setProjectionMatrix(batch.getProjectionMatrix());

        polygonSpriteBatch.begin();
        for(Entity entity: spriteQueue) {
            tC = tM.get(entity);
            dC = dM.get(entity);
            dC.polygonSprite.setPosition(tC.position.x, tC.position.y);
            dC.polygonSprite.setRotation(tC.angle);
            dM.get(entity).polygonSprite.draw(polygonSpriteBatch);
        }
        polygonSpriteBatch.end();

        batch.begin();
        for(Entity entity: textureQueue) {
            tC = tM.get(entity);
            dC = dM.get(entity);
            batch.draw(dC.textureRegion, tC.position.x - tC.width * 0.5f, tC.position.y - tC.height * 0.5f,tC.width * 0.5f, tC.height * 0.5f,
                    tC.width , tC.height, tC.scaleX, tC.scaleY, tC.angle);
        }
        batch.end();

        textureQueue.clear();
        spriteQueue.clear();
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        if(dM.get(entity).textureRegion != null) textureQueue.add(entity);
        if(dM.get(entity).polygonSprite != null) spriteQueue.add(entity);
    }

}
