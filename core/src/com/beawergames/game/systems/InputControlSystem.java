package com.beawergames.game.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.beawergames.game.Game;
import com.beawergames.game.GameScreen;
import com.beawergames.game.components.DrawComponent;
import com.beawergames.game.components.FigureComponent;
import com.beawergames.game.components.TransformComponent;

import java.util.ArrayList;

public class InputControlSystem extends EntitySystem {
    private Engine engine;
    private World world;
    private UISystem uiSystem;
    private float newX, newY;
    private boolean positionUpdated = false;
    private boolean createNextFigure = false;
    private Body currentFigure;
    private Entity currentEntity;

    private float spawnTimer = 0.0f;

    private ComponentMapper<TransformComponent> tM;
    private ComponentMapper<FigureComponent> fM;

    public InputControlSystem(int priority,UISystem uiSystem, Engine engine, World world) {
        super(priority);
        this.engine = engine;
        this.world = world;
        this.uiSystem = uiSystem;

        tM = ComponentMapper.getFor(TransformComponent.class);
        fM = ComponentMapper.getFor(FigureComponent.class);
        createNextFigure();
    }

    @Override
    public void update(float deltaTime) {
        if(createNextFigure) {
            spawnTimer += deltaTime;
            if(spawnTimer > 2) {
                createNextFigure();
                spawnTimer = 0.0f;
            }
        }
        if(positionUpdated && currentFigure != null && !createNextFigure) {
            float dX = newX / GameScreen.PIXELS_IN_METER - currentFigure.getPosition().x;
            float dY = newY / GameScreen.PIXELS_IN_METER - currentFigure.getPosition().y;
              currentFigure.setLinearVelocity(dX * 10, dY * 10);
        }
    }

    public boolean touchDown(int screenX, int screenY) {
        if(createNextFigure) return false;
        if(!uiSystem.getEndPopup() && !uiSystem.getHelpPopup()) {
            changeCoordinates(screenX, screenY);
//            spawnFigure = true;
        }
        return true;
    }

    public void touchDragged(int screenX, int screenY) {
        if(!uiSystem.getEndPopup() && !uiSystem.getHelpPopup() && !createNextFigure) {
            changeCoordinates(screenX, screenY);
        }
    }

    public void touchUp() {
        if(!createNextFigure) {
            if(uiSystem.getHelpPopup()) {
                uiSystem.hideHelpPopup();
            } else if(!uiSystem.getEndPopup()){
                currentFigure.setLinearVelocity(0,-10);
                currentFigure.setType(BodyDef.BodyType.DynamicBody);
                createNextFigure = true;
                //createNextFigure();
            }
            positionUpdated = false;
        }
    }

    private void spawnFigure() {
        TransformComponent tC = tM.get(currentEntity);
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(tC.position.x / GameScreen.PIXELS_IN_METER, tC.position.y / GameScreen.PIXELS_IN_METER);
        bodyDef.type = BodyDef.BodyType.KinematicBody;

        Body body = world.createBody(bodyDef);

        ArrayList<PolygonShape> polygonShapes = fM.get(currentEntity).figure.createPolygonShape();
        for(int i = 0; i < polygonShapes.size(); i++) {
            FixtureDef fixtureDef = new FixtureDef();
            fixtureDef.shape = polygonShapes.get(i);
            fixtureDef.density = 0.1f;
            fixtureDef.friction = 1.5f;
            fixtureDef.restitution = 0.0f;

            body.createFixture(fixtureDef);
            polygonShapes.get(i).dispose();
        }

        body.setUserData(currentEntity);
        currentFigure = body;
    }

    private void changeCoordinates(int screenX, int screenY) {
        newX = (screenX * (float)Game.SCREEN_WIDTH) / Gdx.graphics.getWidth();
        int dY = 0;
        if(currentEntity != null) {
            DrawComponent dC = currentEntity.getComponent(DrawComponent.class);
            if(dC != null) {
                if(dC.polygonSprite != null) {
                    dY = (int) (dC.polygonSprite.getY() - dC.polygonSprite.getBoundingRectangle().y);
                }
            }
        }
        newY = Math.max((Gdx.graphics.getHeight() - screenY) * uiSystem.getScreenHeight() / Gdx.graphics.getHeight() + 5, getHeight() + dY + 5);

        positionUpdated = true;
    }

    private Float getHeight() {
        Array<Body> bodies = new Array<>();
        world.getBodies(bodies);
        Entity entity;
        float output = 0.0f;

        for(Body body: bodies) {
            if(body.getType() != BodyDef.BodyType.KinematicBody) {
                entity = (Entity) body.getUserData();
                DrawComponent dC = entity.getComponent(DrawComponent.class);
                if(dC != null) {
                    if(dC.polygonSprite != null) {
                        output = Math.max(output, dC.polygonSprite.getBoundingRectangle().y + dC.polygonSprite.getBoundingRectangle().height);
                    }
                }
            }
        }
        return output;
    }

    private void createNextFigure() {
        Entity figure = new Entity();

        FigureComponent fC = new FigureComponent();
        TransformComponent tC = new TransformComponent(Game.SCREEN_WIDTH / 2.0f, uiSystem.getCameraHeight() + Game.SCREEN_HEIGHT / 2.0f - 100);
        DrawComponent dC = new DrawComponent(fC.figure.createPolygonSprite());

        figure.add(fC);
        figure.add(tC);
        figure.add(dC);

        engine.addEntity(figure);
        currentEntity = figure;
        spawnFigure();
        createNextFigure = false;
    }
}
