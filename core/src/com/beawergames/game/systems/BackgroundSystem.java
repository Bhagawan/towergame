package com.beawergames.game.systems;

import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.beawergames.game.Assets;
import com.beawergames.game.Game;
import com.beawergames.game.GameScreen;

public class BackgroundSystem extends EntitySystem {
    private OrthographicCamera camera;
    private SpriteBatch batch;
    private Float horizontLine;

    public BackgroundSystem(SpriteBatch spriteBatch, OrthographicCamera camera, int priority)  {
        super(priority);
        this.camera = camera;
        this.batch = spriteBatch;
        horizontLine = GameScreen.PIXELS_IN_METER * 3.0f;
    }

    @Override
    public void update(float deltaTime) {
        batch.begin();
        int half = Assets.sky.getHeight() / 2;
        TextureRegion t = new TextureRegion(Assets.sky, 0,0, Assets.sky.getWidth(), half);

        if(camera.position.y - camera.viewportHeight / 2 <= camera.viewportHeight + horizontLine){
            batch.draw(Assets.terrain, 0,0, Game.SCREEN_WIDTH, horizontLine);
            batch.draw(Assets.sky, 0, horizontLine, camera.viewportWidth, camera.viewportHeight);
            if(camera.position.y + camera.viewportHeight / 2.0f > camera.viewportHeight + horizontLine) batch.draw(t,
                    0, camera.viewportHeight + horizontLine,
                    camera.viewportWidth, camera.position.y + camera.viewportHeight / 2.0f - camera.viewportHeight - horizontLine);
        } else {
            batch.draw(t, camera.position.x - camera.viewportWidth / 2, camera.position.y - camera.viewportHeight / 2, camera.viewportWidth, camera.viewportHeight);
        }
        batch.end();

    }
}
