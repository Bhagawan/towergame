package com.beawergames.game.systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.beawergames.game.Assets;
import com.beawergames.game.Game;
import com.beawergames.game.GameScreen;
import com.beawergames.game.components.DrawComponent;
import com.beawergames.game.util.Records;

import java.util.Locale;

public class UISystem extends EntitySystem {
    private OrthographicCamera camera;
    private SpriteBatch batch;
    private World world;
    private Stage stage;
    private Table table = new Table();
    private Float currentHeight;
    private ShapeRenderer shapeRenderer = new ShapeRenderer();
    private UIInterface uiInterface;

    private Table endPopup = new Table();
    private Table helpPopup = new Table();
    private Label endLabel;
    private int livesAmount = 5;

    public UISystem(SpriteBatch spriteBatch, Stage stage, OrthographicCamera camera, World world, int priority) {
        super(priority);
        this.camera = camera;
        this.batch = spriteBatch;
        this.world = world;
        this.stage = stage;

        table.setFillParent(true);
        stage.addActor(table);

        currentHeight = GameScreen.PIXELS_IN_METER * 2.0f;
        createCloseButton();
        createEndPopup();
        showHelpPopup();
    }

    @Override
    public void update(float deltaTime) {
        updateHeightMark();
        if(currentHeight > camera.position.y) camera.translate(0, 2);

        batch.begin();
        String text = String.format(Locale.getDefault(), "%.2f",currentHeight / GameScreen.PIXELS_IN_METER - 2) + "м";
        Assets.defaultFontRu.setColor(Color.RED);
        Assets.defaultFontRu.draw(batch, text, 0, currentHeight + 20);

        batch.setColor(Color.RED);
        batch.draw(new TextureRegion(Assets.square), camera.position.x + camera.viewportWidth / 2.0f - 30,
                camera.position.y - camera.viewportHeight / 2.0f + 20,
                Assets.square.getWidth() / 2.0f, Assets.square.getHeight() / 2.0f,
                Assets.square.getWidth(), Assets.square.getHeight(), 2.0f, 2.0f, 0.0f);
        batch.setColor(new Color(1, 1, 1, 1));

        Assets.defaultFontRu.setColor(Color.WHITE);
        int amount = livesAmount;
        if(amount < 0) amount = 0;
        Assets.defaultFontRu.draw(batch, String.valueOf(amount) , camera.position.x + camera.viewportWidth / 2.0f - 30,
                camera.position.y - camera.viewportHeight / 2.0f + 35);

        batch.end();
        shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(1, 0, 0, 1);
        shapeRenderer.rectLine(0, currentHeight, Game.SCREEN_WIDTH / 5.0f, currentHeight, 3.0f);
        shapeRenderer.end();

        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();

    }

    @Override
    public void removedFromEngine(Engine engine) {
        shapeRenderer.dispose();
    }

    public void setInterface(UIInterface uiInterface) {
        this.uiInterface = uiInterface;
    }

    private void updateHeightMark() {
        Entity entity;
        DrawComponent dC;
        Array<Body> bodies = new Array<>();
        world.getBodies(bodies);
        for(Body body: bodies) {
            if(body.getLinearVelocity().x == 0 && body.getLinearVelocity().y == 0 && body.getType() == BodyDef.BodyType.DynamicBody) {
                entity = (Entity) body.getUserData();
                dC = entity.getComponent(DrawComponent.class);
                if(dC != null) {
                    if(dC.polygonSprite != null) {
                        Rectangle r = dC.polygonSprite.getBoundingRectangle();
                        if(r.y > currentHeight) currentHeight = r.y;
                        if(r.y + r.height > currentHeight) currentHeight = r.y + r.height;
                    }
                }
            }
        }
    }

    // Public functions

    public void showEndPopup() {
        if(!getEndPopup()) {
            saveRecord(currentHeight / GameScreen.PIXELS_IN_METER - 2);
            table.getCells().removeIndex(1);
            table.row();
            endLabel.setText("Конец! высота вашей башни\n          " +
                    String.format(Locale.getDefault(), "%.2f",currentHeight / GameScreen.PIXELS_IN_METER - 2) +" м");
            table.add(endPopup).center().expand();
        }
    }

    public void hideEndPopup() {
        table.removeActor(endPopup);
    }

    public void hideHelpPopup() {
        table.removeActor(helpPopup);
    }

    public Float getScreenHeight() {
        return camera.viewportHeight;
    }

    public int getLivesAmount() {
        return livesAmount;
    }

    public float getCameraHeight() {return camera.position.y;}

    public void setLivesAmount(int livesAmount) {
        this.livesAmount = livesAmount;
        if(livesAmount <= 0) showEndPopup();
    }

    public boolean getEndPopup() {
        return table.findActor(endPopup.getName()) != null;
    }

    public boolean getHelpPopup() {
        return table.findActor(helpPopup.getName()) != null;
    }

    // Private functions

    private void createCloseButton() {

        Button closeButton = new Button(new TextureRegionDrawable(Assets.btnCloseUp), new TextureRegionDrawable(Assets.btnCloseDown));
        closeButton.addListener(new InputListener(){

            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                if(uiInterface != null) uiInterface.closeGame();
            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                if(uiInterface != null) uiInterface.closeGame();
                return true;
            }
        });

        table.top().add(closeButton).left().top().expandX();
    }

    private void createEndPopup() {
        int width = Game.SCREEN_WIDTH / 2;
        //table.row();
        int height = (int)(Game.SCREEN_HEIGHT * 0.75f);
        endPopup = new Table();
        endPopup.pad(20);

        endPopup.setBounds(Game.SCREEN_WIDTH / 2.0f - width * 0.5f, Game.SCREEN_HEIGHT / 2.0f - height * 0.5f, width, height);
        endPopup.setBackground(new TextureRegionDrawable(Assets.btnUp));

        endLabel = new Label("Конец! высота вашей башни\n          " +
                 String.format(Locale.getDefault(), "%.2f",currentHeight / GameScreen.PIXELS_IN_METER - 2) +" м", new Label.LabelStyle(Assets.fontRu14, Color.WHITE));

        endPopup.setName("endPopup");
        endPopup.center().top().add(endLabel).colspan(2);
        endPopup.row();

        Button closeButton = new Button(new TextureRegionDrawable(Assets.btnExitIdle), new TextureRegionDrawable(Assets.btnExitPressed));
        closeButton.addListener(new InputListener(){
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                if(uiInterface != null) uiInterface.closeGame();
                hideEndPopup();
            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });

        endPopup.add(closeButton).right().padRight(20).padTop(20).size(40,40);

        Button retryButton = new Button(new TextureRegionDrawable(Assets.btnRestartIdle), new TextureRegionDrawable(Assets.btnRestartPressed));
        retryButton.addListener(new InputListener(){
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                currentHeight = GameScreen.PIXELS_IN_METER * 2.0f;
                if(uiInterface != null) uiInterface.restart();
                livesAmount = 5;
                camera.translate(0, camera.viewportHeight / 2.0f - camera.position.y);
                hideEndPopup();
            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });

        endPopup.add(retryButton).left().padLeft(20).padTop(20).size(40,40);
    }

    private void showHelpPopup() {
        int width = Game.SCREEN_WIDTH / 2;
        int height = (int)(Game.SCREEN_HEIGHT * 0.75f);
        table.row();
        table.add(helpPopup).center().expand();
        helpPopup.setBounds(Game.SCREEN_WIDTH / 2.0f - width * 0.5f, Game.SCREEN_HEIGHT / 2.0f - height * 0.5f, width, height);
        helpPopup.setBackground(new TextureRegionDrawable(Assets.btnUp));

        Label endLabel = new Label("Постройте как можно более высокую башню, \nстараясь не уронить блоки мимо платформы.", new Label.LabelStyle(Assets.fontRu14, Color.WHITE));

        helpPopup.setName("helpPopup");
        helpPopup.center().top().add(endLabel).pad(20, 10, 20, 20);
    }

    private void saveRecord(float record) {
        if(Records.isRecord(record)) {
            Pixmap pixmap = Pixmap.createFromFrameBuffer(Gdx.graphics.getWidth() / 4, 0, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
            Records.saveRecord(record, pixmap);
        }
    }


    public interface UIInterface {
        void closeGame();
        void restart();
    }
}
