package com.beawergames.game.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.beawergames.game.GameScreen;
import com.beawergames.game.components.TransformComponent;

public class BodyPositionUpdateSystem extends EntitySystem {
    private World world;
    private Engine engine;
    private ComponentMapper<TransformComponent> tM;
    private BodyUpdateInterface bodyUpdateInterface;

    public BodyPositionUpdateSystem(int priority, World world) {
        super(priority);
        this.world = world;
        tM = ComponentMapper.getFor(TransformComponent.class);
    }

    @Override
    public void addedToEngine(Engine engine) {
        this.engine = engine;
        super.addedToEngine(engine);
    }

    @Override
    public void update(float deltaTime) {
        Array<Body> bodies = new Array<>();
        world.getBodies(bodies);
        Entity entity;
        TransformComponent tC;

        for(Body body: bodies) {
            entity = (Entity) body.getUserData();
            tC = tM.get(entity);
            float off = 0;
            if(body.getPosition().y < -1) {
                if (bodyUpdateInterface != null) bodyUpdateInterface.onDestroy();
                world.destroyBody(body);
                engine.removeEntity(entity);
            } else {
                tC.setPosition(body.getPosition().x * GameScreen.PIXELS_IN_METER - off, body.getPosition().y * GameScreen.PIXELS_IN_METER - off);
                tC.angle = MathUtils.radiansToDegrees * body.getAngle();
            }

        }
    }

    public void setBodyUpdateInterface (BodyUpdateInterface bodyUpdateInterface) {
        this.bodyUpdateInterface = bodyUpdateInterface;
    }

    public interface BodyUpdateInterface {
        void onDestroy();
    }
}
