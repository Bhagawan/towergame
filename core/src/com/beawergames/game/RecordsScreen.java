package com.beawergames.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.beawergames.game.util.Records;

import org.jetbrains.annotations.Nullable;

import java.util.Locale;

public class RecordsScreen extends ScreenAdapter {
    private final Game game;
    private Stage stage;

    public RecordsScreen(Game game) {
        this.game = game;
        stage = new Stage(new ScreenViewport());
        createScreen();
    }

    @Override
    public void render(float delta) {
        ScreenUtils.clear(0, 0, 0, 1);
        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
        stage.clear();
    }

    private void createScreen() {
        Table table = new Table();
        table.setFillParent(true);
        table.setBackground(new TextureRegionDrawable(Assets.sky));
        table.pad(20);
        stage.addActor(table);

        Button backButton = new Button(new TextureRegionDrawable(Assets.backButton));
        backButton.addListener(new InputListener(){
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                game.setScreen(new MenuScreen(game));
            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        table.top().add(backButton).left().top().minWidth(50);
        Label headerLabel = new Label("Рекорды", new Label.LabelStyle(Assets.fontHeader, Color.BROWN));
        table.add(headerLabel).center().expandX();

        float firstRecord = Records.getRecord(Records.FIRST_RECORD);
        if(firstRecord > 0) {
            table.row();
            table.add(createRecordTable("Первое место", firstRecord, Assets.firstRecord)).pad(20).center().colspan(2);
        }
        float secondRecord = Records.getRecord(Records.SECOND_RECORD);
        if(secondRecord > 0) {
            table.row();
            table.add(createRecordTable("Второе место", secondRecord, Assets.secondRecord)).pad(20).center().colspan(2);
        }
        float thirdRecord = Records.getRecord(Records.THIRD_RECORD);
        if(thirdRecord > 0) {
            table.row();
            table.add(createRecordTable("Третье место", thirdRecord, Assets.thirdRecord)).pad(20).center().colspan(2);
        }
    }

    private Table createRecordTable(String header, float record, @Nullable Texture texture) {
        Table table = new Table();
        table.background(new TextureRegionDrawable(Assets.btnUp));
        table.pad(20,40,20,40);

        table.center().top().add(new Label(header, new Label.LabelStyle(Assets.defaultFontRu, Color.WHITE))).center().pad(10).colspan(2);
        table.row();

        Table nestedTable = new Table();
        nestedTable.add(new Label("Высота башни:", new Label.LabelStyle(Assets.defaultFontRu, Color.WHITE))).padBottom(10);
        nestedTable.row();
        nestedTable.add(new Label(String.format(Locale.getDefault(), "%.2f", record), new Label.LabelStyle(Assets.defaultFontRu, Color.WHITE))).padBottom(10);

        table.add(nestedTable).center();
        if(texture != null) {
            Image preview = new Image(texture);
            preview.setOrigin(texture.getWidth() / 8.0f, texture.getHeight() / 8.0f);
            preview.setRotation(180);
            table.add(preview).pad(10,20,10,10).center().size(texture.getWidth() / 4.0f, texture.getHeight() / 4.0f);
        }
        return table;
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}