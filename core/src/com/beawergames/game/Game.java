package com.beawergames.game;

public class Game extends com.badlogic.gdx.Game {
	public static final int SCREEN_WIDTH = 480;
	public static final int SCREEN_HEIGHT = 800;
	public static final double PERFECT_DT = 16.67 * 2;


	@Override
	public void create () {
		Assets.load();
		setScreen(new MenuScreen(this));
	}
}
