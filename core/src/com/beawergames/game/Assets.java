package com.beawergames.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import org.jetbrains.annotations.Nullable;

public class Assets {
    public static Texture btnUp, btnDown;
    public static Texture background, platform, square, sky, terrain, btnCloseUp, btnCloseDown, backButton, recordsBack, btnExitIdle, btnExitPressed, btnRestartIdle, btnRestartPressed;
    public static Texture firstRecord, secondRecord, thirdRecord;
    public static TextButton.TextButtonStyle textButtonStyle;
    public static Skin defaultButtonSkin;
    public static BitmapFont defaultFontRu, fontRu14, fontHeader;

    public static Texture loadTexture (String file) {
        return new Texture(Gdx.files.internal(file));
    }

    public static Texture loadLocalTexture (String file) {
        return new Texture(Gdx.files.local(file));
    }

    public static void load() {
        sky = loadTexture("sky.png");
        terrain = loadTexture("terrain.png");
        btnExitIdle = loadTexture("ButtonExitIdle.png");
        btnExitPressed = loadTexture("ButtonExitSelected.png");
        btnRestartIdle = loadTexture("ButtonRestartIdle.png");
        btnRestartPressed = loadTexture("ButtonRestartSelected.png");
        backButton = loadTexture("backButton.png");
        btnUp = loadTexture("buttonUp.png");
        btnCloseUp = loadTexture("close_button.png");
        btnCloseDown = loadTexture("close_button_pressed.png");
        sky = loadTexture("sky.png");
        btnDown = loadTexture("buttonDown.png");
        background = loadTexture("back.png");
        platform = loadTexture("platform.png");
        platform.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        platform.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        square = loadTexture("square.png");
        square.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        square.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        generateFont();
        createButtonSkin();
        createRecordsBack();
    }

    private static void createButtonSkin() {
        defaultButtonSkin = new Skin();
        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        defaultButtonSkin.add("white", new Texture(pixmap));

        defaultButtonSkin.add("default", new BitmapFont());

        textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.up = new TextureRegionDrawable(btnUp);
        textButtonStyle.down = new TextureRegionDrawable(btnDown);
        textButtonStyle.font = defaultFontRu;
        defaultButtonSkin.add("default", textButtonStyle);
    }

    private static void generateFont() {
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.characters = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
                + "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"
                + "1234567890.,:;_¡!¿?\"'+-*/()[]={}";
        parameter.size = 24;

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator( Gdx.files.internal("defaultRussian.ttf"));
        defaultFontRu = generator.generateFont(parameter);

        parameter.size = 14;
        fontRu14 = generator.generateFont(parameter);

        parameter.size = 54;
        fontHeader = generator.generateFont(parameter);

        generator.dispose();
    }

    private static void createRecordsBack() {
        Pixmap back = new Pixmap(Game.SCREEN_WIDTH, Game.SCREEN_HEIGHT, Pixmap.Format.RGBA8888);

        back.setColor(Color.YELLOW);
        back.fillRectangle(0,0, Game.SCREEN_WIDTH, Game.SCREEN_HEIGHT);
        back.setColor(new Color(0.65f, 0.65f, 0.65f, 1.0f));
        back.fillRectangle(3,3, Game.SCREEN_WIDTH - 6, Game.SCREEN_HEIGHT - 6);

        recordsBack = new Texture(back);

    }

    public static void savePixmap(Pixmap pixmap, String filename) {
        PixmapIO.writePNG(Gdx.files.local(filename + ".png"), pixmap);
    }

    public static void deletePixmap(String filename) {
        Gdx.files.local(filename + ".png").delete();
    }

    public static void loadRecords(@Nullable String first, @Nullable String second, @Nullable String third) {
        if(first != null) {
            if(firstRecord != null) firstRecord.dispose();
            firstRecord = loadLocalTexture(first + ".png");
        }
        if(second != null) {
            if(secondRecord != null) secondRecord.dispose();
            secondRecord = loadLocalTexture(second + ".png");
        }
        if(third != null) {
            if(thirdRecord != null) thirdRecord.dispose();
            thirdRecord = loadLocalTexture(third + ".png");
        }
    }


}
