package com.beawergames.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.FillViewport;

public class MenuScreen extends ScreenAdapter {
    private final Game game;
    private Stage stage;

    public MenuScreen(Game game) {
        this.game = game;
        stage = new Stage(new FillViewport(Game.SCREEN_WIDTH, Game.SCREEN_HEIGHT, new OrthographicCamera(Game.SCREEN_WIDTH, Game.SCREEN_HEIGHT )));
        createMenu();
    }

    @Override
    public void render(float delta) {
        ScreenUtils.clear(0, 0, 0, 1);
        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
        stage.clear();
    }

    private void createMenu() {
        Table table = new Table();
        table.setFillParent(true);
        table.setBackground(new TextureRegionDrawable(Assets.sky));
        stage.addActor(table);

        TextButton gameButton = new TextButton("Новая Игра", Assets.defaultButtonSkin);

        gameButton.addListener(new InputListener(){
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                game.setScreen(new GameScreen(game));
            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });

        table.add(gameButton).size(240, 100).expandY().bottom();
        table.row();

        TextButton recordsButton = new TextButton("Рекорды", Assets.defaultButtonSkin);
        recordsButton.addListener(new InputListener(){
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                game.setScreen(new RecordsScreen(game));
            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        table.add(recordsButton).size(240, 100).expandY().bottom().padBottom(50);
        table.row();

        TextButton exitButton = new TextButton("Выход", Assets.defaultButtonSkin);
        exitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.exit();
            }
        });
        table.add(exitButton).size(240, 100).expandY().bottom().padBottom(50);
    }

    @Override
    public void dispose () {
        stage.dispose();
    }

}
