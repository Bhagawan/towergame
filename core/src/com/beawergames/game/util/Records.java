package com.beawergames.game.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.Json;
import com.beawergames.game.Assets;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Records {
    public static final byte FIRST_RECORD = 1;
    public static final byte SECOND_RECORD = 2;
    public static final byte THIRD_RECORD = 3;

    public static void saveRecord(float record, Pixmap screenshot) {
        Preferences prefs = Gdx.app.getPreferences("Records");

        float firstRecord = prefs.getFloat("firstRecord", 0);
        float secondRecord = prefs.getFloat("secondRecord", 0);
        float thirdRecord = prefs.getFloat("thirdRecord", 0);

        String firstPhoto = prefs.getString("firstPhoto", "");
        String secondPhoto = prefs.getString("secondPhoto", "");
        String thirdPhoto = prefs.getString("thirdPhoto", "");

        if(!thirdPhoto.equals(""))Assets.deletePixmap(thirdPhoto);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyy_hhmmss_SSS", Locale.getDefault());
        String filename = "File_" + simpleDateFormat.format(new Date());
        Assets.savePixmap(screenshot, filename);

        if(record > firstRecord) {
            prefs.putFloat("thirdRecord", secondRecord);
            if(!secondPhoto.equals("")) prefs.putString("thirdPhoto", secondPhoto);

            prefs.putFloat("secondRecord", firstRecord);
            if(!firstPhoto.equals("")) prefs.putString("secondPhoto", firstPhoto);

            prefs.putFloat("firstRecord", record);
            prefs.putString("firstPhoto", filename);
        } else if(record > secondRecord) {
            prefs.putFloat("thirdRecord", secondRecord);
            if(!secondPhoto.equals("")) prefs.putString("thirdPhoto", secondPhoto);

            prefs.putFloat("secondRecord", record);
            prefs.putString("secondPhoto", filename);
        } else if(record > thirdRecord) {
            prefs.putFloat("thirdRecord", record);
            prefs.putString("thirdPhoto", filename);
        }
        prefs.flush();
        screenshot.dispose();
    }

    public static boolean isRecord(float record) {
        Preferences prefs = Gdx.app.getPreferences("Records");
        float third = prefs.getFloat("thirdRecord", 0);
        if(third > 0) return record > third;
        else return true;
    }

    public static float getRecord(byte record) {
        Preferences prefs = Gdx.app.getPreferences("Records");
        switch (record) {
            case FIRST_RECORD:
                String firstPhoto = prefs.getString("firstPhoto", "");
                if(!firstPhoto.equals("")) Assets.loadRecords(firstPhoto, null, null);
                return prefs.getFloat("firstRecord", 0);
            case SECOND_RECORD:
                String secondPhoto = prefs.getString("secondPhoto", "");
                if(!secondPhoto.equals("")) Assets.loadRecords(null, secondPhoto, null);
                return prefs.getFloat("secondRecord", 0);
            case THIRD_RECORD:
                String thirdPhoto = prefs.getString("thirdPhoto", "");
                if(!thirdPhoto.equals("")) Assets.loadRecords(null, null, thirdPhoto);
                return prefs.getFloat("thirdRecord", 0);
        }
        return 0.0f;
    }


}
