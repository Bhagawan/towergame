package com.beawergames.game.util;

import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.beawergames.game.Assets;
import com.beawergames.game.GameScreen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Figure {
    private ArrayList<MyPoint> figure = new ArrayList<>();

    public Figure() {
        generateFigure();
    }

    public void generateFigure() {
        for(int i = 0; i < 3; i++) {
            List<MyPoint> available = getAvailablePlaces(figure);
            figure.add(available.get(new Random().nextInt(available.size())));
        }
    }

    private List<MyPoint> getAvailablePlaces(List<MyPoint> current) {
        ArrayList<MyPoint> output = new ArrayList<>();
        output.add(new MyPoint(0,1));
        output.add(new MyPoint(1,0));
        output.add(new MyPoint(0,-1));
        output.add(new MyPoint(-1,0));

        if(current.size() > 0) {
            for(int i = 0; i < current.size(); i++) {
                if(output.contains(current.get(i))) output.remove(current.get(i));
                if(!output.contains(new MyPoint(current.get(i).x, current.get(i).y + 1))) output.add(new MyPoint(current.get(i).x, current.get(i).y + 1));
                if(!output.contains(new MyPoint(current.get(i).x + 1, current.get(i).y))) output.add(new MyPoint(current.get(i).x + 1, current.get(i).y));
                if(!output.contains(new MyPoint(current.get(i).x, current.get(i).y - 1))) output.add(new MyPoint(current.get(i).x, current.get(i).y - 1));
                if(!output.contains(new MyPoint(current.get(i).x - 1, current.get(i).y))) output.add(new MyPoint(current.get(i).x - 1, current.get(i).y));
            }
        }
        output.remove(new MyPoint(0,0));
        return output;
    }

    public PolygonSprite createPolygonSprite() {
        float boxSize = Assets.square.getWidth();

        ArrayList<MyPoint> verticesArray = new ArrayList<>();
        ArrayList<Short> trianglesArray = new ArrayList<>();
        verticesArray.add(new MyPoint(0,0));
        verticesArray.add(new MyPoint(0,1));
        verticesArray.add(new MyPoint(1,0));
        verticesArray.add(new MyPoint(1,1));

        Collections.addAll(trianglesArray, (short)verticesArray.indexOf(new MyPoint(0,0))
                , (short)verticesArray.indexOf(new MyPoint(1,0))
                , (short)verticesArray.indexOf(new MyPoint(1,1)));
        Collections.addAll(trianglesArray, (short)verticesArray.indexOf(new MyPoint(0,0))
                , (short)verticesArray.indexOf(new MyPoint(1,1))
                , (short)verticesArray.indexOf(new MyPoint(0,1)));

        for(int i = 0; i < figure.size(); i++) {
            MyPoint bL = new MyPoint(figure.get(i).x, figure.get(i).y);
            MyPoint bR = new MyPoint(figure.get(i).x + 1, figure.get(i).y);
            MyPoint tL = new MyPoint(figure.get(i).x, figure.get(i).y + 1);
            MyPoint tR = new MyPoint(figure.get(i).x + 1, figure.get(i).y + 1);

            if(!verticesArray.contains(bL)) verticesArray.add(bL);
            if(!verticesArray.contains(bR)) verticesArray.add(bR);
            if(!verticesArray.contains(tL)) verticesArray.add(tL);
            if(!verticesArray.contains(tR)) verticesArray.add(tR);

            Collections.addAll(trianglesArray, (short)verticesArray.indexOf(bL)
                    , (short)verticesArray.indexOf(bR)
                    , (short)verticesArray.indexOf(tR));
            Collections.addAll(trianglesArray, (short)verticesArray.indexOf(bL)
                    , (short)verticesArray.indexOf(tR)
                    , (short)verticesArray.indexOf(tL));
        }


        float[] spriteVertices = new float[verticesArray.size() * 2];
        for(int i = 0; i < verticesArray.size(); i++) {
            spriteVertices[i * 2] = verticesArray.get(i).x * boxSize;
            spriteVertices[i * 2 + 1] = verticesArray.get(i).y * boxSize;
        }

        short[] triangles = new short[trianglesArray.size()];
        for(int i = 0; i < trianglesArray.size(); i++) triangles[i] = trianglesArray.get(i);


        PolygonRegion region = new PolygonRegion(new TextureRegion(Assets.square), spriteVertices, triangles);
        PolygonSprite polygonSprite = new PolygonSprite(region);
        polygonSprite.setSize(polygonSprite.getWidth() / boxSize * GameScreen.PIXELS_IN_METER, polygonSprite.getHeight() / boxSize * GameScreen.PIXELS_IN_METER);

        polygonSprite.setOrigin(0,0);
        return polygonSprite;
    }

    public ArrayList<PolygonShape> createPolygonShape() {
        ArrayList<PolygonShape> output = new ArrayList<>();
        ArrayList<MyPoint> fullFigure = new ArrayList<>(figure);
        fullFigure.add(new MyPoint(0,0));

        ArrayList<Vector2> verticesArray = new ArrayList<>();
        verticesArray.add(new Vector2(0,0));
        verticesArray.add(new Vector2(0,1));
        verticesArray.add(new Vector2(1,0));
        verticesArray.add(new Vector2(1,1));

        for(int i = 0; i < figure.size(); i++) {
            Vector2 bL = new Vector2(figure.get(i).x, figure.get(i).y);
            Vector2 bR = new Vector2(figure.get(i).x + 1, figure.get(i).y);
            Vector2 tL = new Vector2(figure.get(i).x, figure.get(i).y + 1);
            Vector2 tR = new Vector2(figure.get(i).x + 1, figure.get(i).y + 1);

            if(!verticesArray.contains(bL)) verticesArray.add(bL);
            if(!verticesArray.contains(bR)) verticesArray.add(bR);
            if(!verticesArray.contains(tL)) verticesArray.add(tL);
            if(!verticesArray.contains(tR)) verticesArray.add(tR);
        }

        for(int i = 0 ; i < verticesArray.size(); i++) {
            Vector2 p = verticesArray.get(i);
            if(fullFigure.contains(new MyPoint(p.x, p.y))
                    && fullFigure.contains(new MyPoint(p.x - 1, p.y))
                    && fullFigure.contains(new MyPoint(p.x, p.y - 1))
                    && fullFigure.contains(new MyPoint(p.x - 1, p.y - 1))) verticesArray.remove(p);
            if(fullFigure.contains(new MyPoint(p.x, p.y))
                    && fullFigure.contains(new MyPoint(p.x, p.y - 1))
                    && !fullFigure.contains(new MyPoint(p.x - 1, p.y))
                    && !fullFigure.contains(new MyPoint(p.x - 1, p.y - 1))) verticesArray.remove(p);
            if(!fullFigure.contains(new MyPoint(p.x, p.y))
                    && !fullFigure.contains(new MyPoint(p.x, p.y - 1))
                    && fullFigure.contains(new MyPoint(p.x - 1, p.y))
                    && fullFigure.contains(new MyPoint(p.x - 1, p.y - 1))) verticesArray.remove(p);
        }
        ArrayList<Float> xCoords = new ArrayList<>();
        ArrayList<Float> yCoords = new ArrayList<>();

        for(int i = 0; i < verticesArray.size(); i++) {
            if(!xCoords.contains(verticesArray.get(i).x)) xCoords.add(verticesArray.get(i).x);
            if(!yCoords.contains(verticesArray.get(i).y)) yCoords.add(verticesArray.get(i).y);
        }
        Collections.sort(xCoords);
        Collections.sort(yCoords, Collections.<Float>reverseOrder());

        for(int y = 1; y < yCoords.size(); y++) {
             float width = 0.0f;
            float height = yCoords.get(y - 1) - yCoords.get(y);
            for(int x = 0; x < xCoords.size(); x++) {
                if(fullFigure.contains(new MyPoint(xCoords.get(x), yCoords.get(y)))) width++;
                else if(width > 0) {
                    Vector2[] vertices = new Vector2[4];
                    vertices[0] = new Vector2(xCoords.get(x) - width, yCoords.get(y));
                    vertices[1] = new Vector2(xCoords.get(x), yCoords.get(y));
                    vertices[2] = new Vector2(xCoords.get(x), yCoords.get(y) + height);
                    vertices[3] = new Vector2(xCoords.get(x) - width, yCoords.get(y) + height);
                    PolygonShape polygonShape = new PolygonShape();
                    polygonShape.set(vertices);
                    output.add(polygonShape);
                    width = 0.0f;
                }
            }
        }

        return output;
    }
}
