package com.beawergames.game.util;

import java.awt.Point;

public class MyPoint{
    public int x = 0;
    public int y = 0;

    public MyPoint() {
        this(0, 0);
    }

    public MyPoint(Point p) {
        this(p.x, p.y);
    }

    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public MyPoint(float x, float y) {
        this.x = (int) x;
        this.y = (int) y;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof MyPoint) return ((MyPoint) obj).x == this.x && ((MyPoint) obj).y == this.y;
        else return false;
    }
}
