package com.beawergames.game;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.beawergames.game.components.DrawComponent;
import com.beawergames.game.components.TransformComponent;
import com.beawergames.game.systems.BackgroundSystem;
import com.beawergames.game.systems.BodyPositionUpdateSystem;
import com.beawergames.game.systems.InputControlSystem;
import com.beawergames.game.systems.RenderSystem;
import com.beawergames.game.systems.UISystem;

public class GameScreen extends ScreenAdapter {
    public static final int GAME_WIDTH = 24;
    public static final int GAME_HEIGHT = 40;
    public static final int PIXELS_IN_METER = Game.SCREEN_WIDTH / GAME_WIDTH;
    private float accumulator = 0;
    private final Game game;
    private Engine engine = new Engine();
    private OrthographicCamera camera;
    private ScreenViewport viewport;
    private Stage stage;
    public SpriteBatch batch;
    private World world = new World(new Vector2(0, -10), true);
    Box2DDebugRenderer debugRenderer = new Box2DDebugRenderer();

    private InputControlSystem inputControlSystem;

    private boolean physicsCamera = false;

    public GameScreen(final Game game) {
        this.game = game;
        stage = new Stage(new FillViewport(Game.SCREEN_WIDTH, Game.SCREEN_HEIGHT, new OrthographicCamera(Game.SCREEN_WIDTH, Game.SCREEN_HEIGHT )));
        batch = new SpriteBatch();

        camera = new OrthographicCamera();

        viewport = new ScreenViewport(camera);
        viewport.setUnitsPerPixel((float)Game.SCREEN_WIDTH / Gdx.graphics.getWidth());
        viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);

        camera.update();
        batch.setProjectionMatrix(camera.combined);

        final UISystem uiSystem = new UISystem(batch, stage, camera, world, 4);
        uiSystem.setInterface(new UISystem.UIInterface() {
            @Override
            public void closeGame() {
                game.setScreen(new MenuScreen(game));
            }

            @Override
            public void restart() {
                restartGame();
            }
        });

        BodyPositionUpdateSystem bodyPositionUpdateSystem = new BodyPositionUpdateSystem( 6, world);
        bodyPositionUpdateSystem.setBodyUpdateInterface(new BodyPositionUpdateSystem.BodyUpdateInterface() {
            @Override
            public void onDestroy() {
                uiSystem.setLivesAmount(uiSystem.getLivesAmount() - 1);
            }
        });

        inputControlSystem = new InputControlSystem(8, uiSystem, engine, world);

        engine.addSystem(new BackgroundSystem(batch, camera, 1));
        engine.addSystem(new RenderSystem(batch, 3));
        engine.addSystem(uiSystem);
        engine.addSystem(bodyPositionUpdateSystem);
        engine.addSystem(inputControlSystem);
        initGame();
    }

    @Override
    public void render(float delta) {
        ScreenUtils.clear(0, 0, 0, 1);
        camera.update();
        batch.setProjectionMatrix(camera.combined);

        engine.update(delta);
        float frameTime = Math.min(delta, 0.25f);
        accumulator += frameTime;
        while (accumulator >= 1.0f/60) {
            world.step(1.0f/60, 6, 6);
            accumulator -= 1.0f/60;
        }
        if(physicsCamera) debugRenderer.render(world, camera.combined);
    }

    @Override
    public void show() {
        InputAdapter custom = new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if(keycode == Input.Keys.SPACE) {
                    if(physicsCamera) {
                        viewport.setUnitsPerPixel((float)Game.SCREEN_WIDTH / Gdx.graphics.getWidth());
                        viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
                        batch.setProjectionMatrix(camera.combined);
                        physicsCamera = false;
                    } else {
                        viewport.setUnitsPerPixel((float)GAME_WIDTH / Gdx.graphics.getWidth());
                        viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
                        batch.setProjectionMatrix(camera.combined);
                         physicsCamera = true;
                    }
                }
                if(keycode == Input.Keys.UP) {
                    camera.position.y += 20;
                }
                if(keycode == Input.Keys.DOWN) {
                    camera.position.y -= 20;
                }
                return true;
            }

            @Override
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                return inputControlSystem.touchDown(screenX, screenY);
            }

            @Override
            public boolean touchDragged(int screenX, int screenY, int pointer) {
                inputControlSystem.touchDragged(screenX, screenY);
                return true;
            }

            @Override
            public boolean touchUp(int screenX, int screenY, int pointer, int button) {
                inputControlSystem.touchUp();
                return true;
            }
        };
        InputMultiplexer inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(stage);
        inputMultiplexer.addProcessor(custom);
        Gdx.input.setInputProcessor(inputMultiplexer);
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
        stage.clear();
    }

    @Override
    public void dispose () {
        batch.dispose();
    }

    private void initGame() {
        createPlatform();
    }

    private void createPlatform() {
        int platformWidth = GAME_WIDTH / 2;
        int platformHeight = 2;

        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set((float)GAME_WIDTH / 2, 0);

        Body body = world.createBody(bodyDef);

        float[] vertices = {
                - platformWidth / 2.0f, platformHeight,
                - platformWidth / 2.0f + 1, platformHeight,
                - platformWidth / 2.0f + 1, platformHeight / 2.0f,
                platformWidth / 2.0f - 1, platformHeight / 2.0f,
                platformWidth / 2.0f - 1, platformHeight,
                platformWidth / 2.0f, platformHeight,
                platformWidth / 2.0f, -platformHeight,
                - platformWidth / 2.0f, -platformHeight
        };

        ChainShape chainShape = new ChainShape();
        chainShape.createLoop(vertices);

        body.createFixture(chainShape, 0.0f);

        for(int  i = 0; i < vertices.length; i++) {
            vertices[i] *= PIXELS_IN_METER;
        }

        short[] triangles = new EarClippingTriangulator().computeTriangles(vertices).toArray();
        PolygonRegion region = new PolygonRegion(new TextureRegion(Assets.platform), vertices, triangles);

        Entity platform = new Entity();
        TransformComponent tC = new TransformComponent((float)GAME_WIDTH / 2, 0);
        tC.setSize(platformWidth * PIXELS_IN_METER, platformHeight * PIXELS_IN_METER);
        DrawComponent dC = new DrawComponent(new PolygonSprite(region));
        platform.add(tC);
        platform.add(dC);
        engine.addEntity(platform);

        body.setUserData(platform);
        chainShape.dispose();
    }

    private void restartGame() {
        Array<Body> bodies = new Array<>();
        world.getBodies(bodies);
        Entity entity;

        for(Body body: bodies) {
            if(body.getType() != BodyDef.BodyType.StaticBody) {
                if(!world.isLocked()) {
                    entity = (Entity) body.getUserData();
                    engine.removeEntity(entity);
                    world.destroyBody(body);
                }
            }
        }
    }

}