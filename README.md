# Tower Game
This game made entirely in OpenGL. 
Scene2d for menus, Box2d in game and Ashley for overall architecture.

<img src="https://gitlab.com/Bhagawan/towergame/-/raw/master/Game.png" width="272" height ="480">
<img src="https://gitlab.com/Bhagawan/towergame/-/raw/master/records.png" width="272" height ="480">
